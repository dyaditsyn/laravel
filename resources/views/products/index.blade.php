@extends('layouts.main')

@section('content')
<div class="album py-5 bg-light">
    <div class="container-fluid">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="row">
            <div class="col-md-2">
                @include('products.categories', ['categories' => $categories, 'categoryId' => $categoryId])
            </div>
            <div class="col-md-10">
                <form method="post" action="{{route('cart.add')}}">
                    @csrf
                    <div class="row">
                        @foreach ($products as $product)
                        @include('products.product', ['product' => $product])
                        @endforeach
                    </div>
                    <input type="submit" class="btn btn-success" value="Order" />
                </form>
                <br />
                {{ $products->links() }}
            </div>
        </div>
    </div>
</div>
@endsection