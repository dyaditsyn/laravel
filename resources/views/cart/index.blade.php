@extends('layouts.main')

@section('content')
<div class="album py-5 bg-light">
    <div class="container">
        <div class="row">
            @foreach ($cart->products as $product)
            @include('cart.product', ['product' => $product])
            @endforeach
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                Total:
            </div>
            <div class="col-md-6 text-end">
                <b>$@convert($totalPrice)</b>
            </div>
        </div>
    </div>
</div>
@endsection