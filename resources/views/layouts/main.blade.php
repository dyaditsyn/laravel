<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        @guest

        @else
        <div class="row">
            <div class="col-lg-12">
                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                    @csrf
                    <input type="submit" class="btn btn-warning my-2 float-end" value="Logout">
                    <a href="{{route('profile')}}" class=" btn btn-primary my-2 mx-1 float-end">Profile</a>
                </form>

            </div>
        </div>
        @endguest
    </div>

    <main role="main" class="py-4">
        <section class="jumbotron text-center">
            <div class="container">
                <h1>Best shop</h1>
                <p class="lead text-muted">Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don’t simply skip over it entirely.</p>
                <p>
                    @if(url()->current() == route('products'))
                    <a href="#" class=" btn btn-primary my-2">All products</a>
                    <a href="{{route('cart')}}" class="btn btn-secondary my-2">Shopping cart</a>
                    @elseif(url()->current() == route('cart'))
                    <a href="{{route('products')}}" class=" btn btn-secondary my-2">All products</a>
                    <a href="#" class="btn btn-primary my-2">Shopping cart</a>
                    @endif
                </p>
            </div>
        </section>
        @yield('content')
    </main>
    </div>

    <footer class="text-muted">
        <div class="container">
            <p class="float-right">
                <a href="#">Back to top</a>
            </p>
            <p>This is test shop!</p>
            <p>New to Bootstrap? <a href="https://getbootstrap.com/">Visit the homepage</a> or read our <a href="/docs/4.5/getting-started/introduction/">getting started guide</a>.</p>
        </div>
    </footer>
</body>

</html>