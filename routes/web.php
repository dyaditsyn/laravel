<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminMainController;
use App\Http\Controllers\AdminProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth', 'user.check'])->group(function () {
    Route::get('/{category_id?}', [ProductController::class, 'index'])->name('products')->where('category_id', '[0-9]+');

    Route::prefix('profile')->group(
        function () {
            Route::get('/', [UserController::class, 'profile'])->name('profile');
            Route::post('/update', [UserController::class, 'update'])->name('profile.update');
        }
    );

    Route::prefix('cart')->group(
        function () {
            Route::get('/', [CartController::class, 'index'])->name('cart')->middleware('cart.exist');
            Route::post('/add', [CartController::class, 'add'])->name('cart.add');
        }
    );
});

Route::middleware(['auth', 'admin.check'])->group(function () {
    Route::prefix('admin')->group(
        function () {
            Route::get('/', [AdminMainController::class, 'index'])->name('admin');
            Route::resource('products', AdminProductController::class);
        }
    );
});
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');
//Route::get('/admin', [App\Http\Controllers\HomeController::class, 'admin'])->name('admin')->middleware('auth.type:admin');
