<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Category;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $productName = $this->faker->productName;
        return [
            'price' => $this->faker->numberBetween($min = 100, $max = 9000),
            'quantity' => $this->faker->numberBetween($min = 1, $max = 100),
            'category_id' => Category::inRandomOrder()->value('id'),
            'name' => $productName,
            'image' => $this->faker->imageURL($width = 640, $height = 480, '', true, $productName),
        ];
    }
}
