<?php


namespace App\Services;

use App\Services\BaseService;
use App\Repositories\ProductRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductService extends BaseService
{
    public function __construct(ProductRepository $repo)
    {
        $this->repo = $repo;
    }

    public function getByCategoryWithPagination(string $categoryId): LengthAwarePaginator
    {
        return $this->repo->getByCategoryWithPagination($categoryId);
    }
}
