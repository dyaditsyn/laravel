<?php


namespace App\Services;

use App\Services\BaseService;
use App\Repositories\UserRepository;

class UserService extends BaseService
{
    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }

    public function activeUsersCount(): int
    {
        return $this->repo->activeUsersCount();
    }
}
