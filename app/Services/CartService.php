<?php


namespace App\Services;

use App\Services\BaseService;
use App\Repositories\CartRepository;

class CartService extends BaseService
{
    public function __construct(CartRepository $repo)
    {
        $this->repo = $repo;
    }

    public function calcTotalProductPrice(string $id): float
    {
        $cart = $this->repo->findById($id);
        $sum = 0;
        foreach ($cart->products as $product) {
            $sum += $product->price * $product->pivot->quantity;
        }
        return $sum;
    }

    public function clearCart(string $id)
    {
        foreach ($this->repo->findById($id)->products as $product) {
            $product->pivot->delete();
        }
        return true;
    }
}
