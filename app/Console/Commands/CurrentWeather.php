<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CurrentWeather extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'current:weather
                                        {city? : Selected city you want weather to display}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Displays weather data in the selected city';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $city = $this->argument('city') ?? '';
        while (empty($city)) {
            $this->error('City must be non-empty string!');
            $city = $this->ask('In which city you whant to know weater?');
        }
        $this->info('Your request is being processed...');

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.openweathermap.org/data/2.5/weather?q={$city}&appid=" . env("WEATHER_API_KEY") . "&units=metric",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $res = (json_decode($response, true));
            if (isset($res['main'])) {
                echo "Local temperature in " . $city . " is " . $res['main']['temp'] . " degrees Celsius";
                echo "\n";
            } else {
                echo $city . " is invalid. Plese re-check your input and try again";
                echo "\n";
            }
        }
    }
}
