<?php


namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\CartProducts;


class CartProductRepository extends BaseRepository
{
    public function __construct(CartProducts $model)
    {
        $this->model = $model;
    }
}
