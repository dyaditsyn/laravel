<?php


namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Cart;


class CartRepository extends BaseRepository
{
    public function __construct(Cart $model)
    {
        $this->model = $model;
    }
}
