<?php


namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\Interfaces\BaseInterface;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class BaseRepository implements BaseInterface
{
    protected $model;

    public $sortBy = 'id';
    public $sortOrder = 'desc';
    public $count = 10;

    /**
     * Repo Constructor
     * Override to clarify typehinted model.
     * @param Model $model Repo DB ORM Model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get all instances of model
     *
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model
            ->orderBy($this->sortBy, $this->sortOrder)
            ->get();
    }

    /**
     * Get all instances of model with pagination
     *
     * @return Collection
     */
    public function allWithPagination(): LengthAwarePaginator
    {
        return $this->model
            ->paginate($this->count);
    }

    /**
     * Create a new record in the database
     *
     * @param array $data
     * @return model
     */
    public function create(array $data)
    {
        // try {
        //     $model = $this->model->create($data);
        // } catch (Throwable $th) {
        //     // log here
        // }
        // return $model;
        return $this->model->create($data);
    }

    /**
     * Update record in the database and get data back
     *
     * @param int $id
     * @param array $data
     * @return booleam
     */
    public function update(int $id, array $data): bool
    {
        // try {
        //     $query = $this->model->where('id', $id)->first();
        //     $result = $query->update($data);
        // } catch (Throwable $th) {
        //     // log here
        // }
        // return $result;
        $query = $this->model->where('id', $id);
        return $query->update($data);
    }

    /**
     * Remove record in the database
     *
     * @param int $id
     * @return booleam
     */
    public function destroy(int $id): bool
    {
        // try {
        //     $this->model->destroy('id');
        // } catch (Throwable $th) {
        //     // log here
        // }
        $this->model->destroy('id');
        return true;
    }

    /**
     * Show the record with the given id
     *
     * @param int $id
     * @return model
     */
    public function findById(int $id)
    {
        // try {
        //     $model = $this->model->find('id');
        // } catch (Throwable $th) {
        //     // log here
        // }
        // return $model;
        return $this->model->find($id);
    }

    /**
     * Get the associated model
     *
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->model;
    }

    /**
     * Set the associated model
     *
     * @param $model
     * @return $this
     */
    public function setModel(Model $model)
    {
        $this->model = $model;
        return true;
    }

    /**
     * Get count of model
     *
     * @return integer
     */
    public function count(): int
    {
        return $this->model->count();
    }
}
