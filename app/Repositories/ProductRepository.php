<?php


namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Product;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductRepository extends BaseRepository
{
    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function getByCategoryWithPagination(string $categoryId): LengthAwarePaginator
    {
        return $this->model->where("category_id", $categoryId)
            ->paginate($this->count);
    }
}
