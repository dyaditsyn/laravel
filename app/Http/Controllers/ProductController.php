<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Cart;
use App\Models\CartProducts;
use App\Models\Category;
use Helper;
use App\Services\ProductService;
use App\Services\CategoryService;

class ProductController extends Controller
{
    public function __construct(ProductService $products, CategoryService $categories)
    {
        $this->products = $products;
        $this->categories = $categories;
    }

    public function index(Request $request, $categoryId = null)
    {
        if (empty($categoryId)) {
            $products = $this->products->getAllWithPagination();
        } else {
            $products = $this->products->getByCategoryWithPagination($categoryId);
        }
        $categories = $this->categories->all();
        $title = Helper::shout("Product page");
        return view('products.index', ["products" => $products, "categories" => $categories, "categoryId" => $categoryId]);
    }
}
