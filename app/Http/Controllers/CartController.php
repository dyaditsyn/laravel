<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\CartProducts;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\AddCartProductsRequest;
use App\Services\CartService;
use App\Services\CartProductService;

class CartController extends Controller
{
    public function __construct(CartService $cart, CartProductService $cartProduct)
    {
        $this->cart = $cart;
        $this->cartProduct = $cartProduct;
    }

    public function index(Request $request)
    {
        $cart = $this->cart->findById(session('cart_id') ?? 0);
        return view(
            'cart.index',
            [
                "cart" => $cart,
                "totalPrice" => $this->cart->calcTotalProductPrice($cart->id)
            ]
        );
    }

    public function add(AddCartProductsRequest $request)
    {
        if (session('cart_id')) {
            $this->cart->clearCart(session('cart_id'));
        } else {
            $cart = $this->cart->create(["user_id" => Auth::id()]);
            session(['cart_id' => $cart->id]);
        }
        $products = $request->input('products');

        foreach ($products as $product) {
            // $validator = Validator::make(['product_id' => $product, 'cart_id' => $cartId], [
            //     'product_id' => [
            //         'required',
            //         Rule::unique('cart_products')->where(function ($query) use ($product, $cartId) {
            //             return $query->where('cart_id', $cartId)->where('product_id', $product);
            //         })
            //     ]
            // ]);
            $quantity = $request->input('quantity_' . $product);
            $this->cartProduct->create(
                [
                    "cart_id" => session('cart_id'),
                    "product_id" => $product,
                    "quantity" => $quantity
                ]
            );
        }
        return redirect()->route('cart');
    }
}
